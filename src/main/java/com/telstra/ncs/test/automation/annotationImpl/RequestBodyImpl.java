package com.telstra.ncs.test.automation.annotationImpl;

import com.telstra.ncs.test.automation.annotation.RequestBody;
import com.telstra.ncs.test.automation.domain.HttpBean;
import com.telstra.ncs.test.automation.util.PropertyUtil;
import org.json.simple.parser.JSONParser;
import org.junit.runners.model.FrameworkMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.IOException;

/**
 * Created by David.Zheng on 10/09/2015.
 */
@Service("requestBody")
public class RequestBodyImpl implements UnitTestAnnotation
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestBodyImpl.class);

	public void processAnnotationByMethod(HttpBean httpBean, FrameworkMethod method)
	{
		RequestBody requestBody = method.getAnnotation(RequestBody.class);
		process(httpBean, requestBody);
	}

	private void process(HttpBean httpBean, RequestBody requestBody)
	{
		FileReader fileReader = null;
		if (requestBody != null)
		{
			try
			{
				String key = requestBody.value();
				String location = PropertyUtil.getProperty(key);
				if (location != null)
				{
					JSONParser parser = new JSONParser();
					fileReader = new FileReader(PropertyUtil.getCurrentFolder() + location);
					Object obj = parser.parse(fileReader);
					httpBean.setPayload(obj.toString());
				}
				else
				{
					httpBean.setPayload(key);
				}
			}
			catch (Exception e)
			{
				LOGGER.debug(e.getMessage());
				throw new RuntimeException(e.getMessage(), e.getCause());
			}
			finally
			{
				if (fileReader != null)
				{
					try
					{
						fileReader.close();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		}
	}
}
