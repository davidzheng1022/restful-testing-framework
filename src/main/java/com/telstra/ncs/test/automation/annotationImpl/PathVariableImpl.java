package com.telstra.ncs.test.automation.annotationImpl;

import com.telstra.ncs.test.automation.annotation.PathVariable;
import com.telstra.ncs.test.automation.domain.HttpBean;
import com.telstra.ncs.test.automation.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.runners.model.FrameworkMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by David on 9/24/15.
 */
@Service("pathVariable")
public class PathVariableImpl implements UnitTestAnnotation
{
	private static final Logger LOGGER = LoggerFactory.getLogger(PathVariableImpl.class);

	@Override public void processAnnotationByMethod(HttpBean httpBean, FrameworkMethod method)
	{
		PathVariable pathVariable = method.getAnnotation(PathVariable.class);
		process(httpBean, pathVariable);
	}

	private void process(HttpBean httpBean, PathVariable pathVariable)
	{
		if (pathVariable != null)
		{
			try
			{
				String param = pathVariable.name();
				String key = pathVariable.value();
				String paramValue = StringUtils.defaultString(PropertyUtil.getProperty(key), key);
				String endPoint = httpBean.getEndPoint();
				httpBean.setEndPoint(StringUtils.replace(endPoint, "{" + param + "}", paramValue));
			}
			catch (Exception e)
			{
				LOGGER.debug(e.getMessage());
				throw new RuntimeException(e.getMessage(), e.getCause());
			}
		}
	}


}
