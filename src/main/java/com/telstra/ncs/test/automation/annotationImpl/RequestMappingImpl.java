package com.telstra.ncs.test.automation.annotationImpl;

import com.telstra.ncs.test.automation.annotation.RequestMapping;
import com.telstra.ncs.test.automation.domain.HttpBean;
import org.junit.runners.model.FrameworkMethod;
import org.springframework.stereotype.Service;

/**
 * Created by David on 10/09/2015.
 */
@Service("requestMapping")
public class RequestMappingImpl implements UnitTestAnnotation
{

	public void processAnnotationByMethod(HttpBean httpBean, FrameworkMethod method)
	{
		RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
		process(httpBean, requestMapping);
	}

	private void process(HttpBean httpBean, RequestMapping requestMapping)
	{
		if (requestMapping != null)
		{
			String uri = requestMapping.uri();
			String httpMethod = requestMapping.method();
			httpBean.setEndPoint(System.getProperty("host") + uri);
			httpBean.setMethod(httpMethod);
		}
	}
}
