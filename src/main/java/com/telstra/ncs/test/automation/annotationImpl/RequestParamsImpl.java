package com.telstra.ncs.test.automation.annotationImpl;

import com.telstra.ncs.test.automation.domain.HttpBean;
import com.telstra.ncs.test.automation.annotation.RequestParam;
import com.telstra.ncs.test.automation.annotation.RequestParams;
import com.telstra.ncs.test.automation.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.runners.model.FrameworkMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by David on 11/09/2015.
 */
@Service("requestParams")
public class RequestParamsImpl implements UnitTestAnnotation
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestParamsImpl.class);

	public void processAnnotationByMethod(HttpBean httpBean, FrameworkMethod method)
	{
		RequestParams requestParams = method.getAnnotation(RequestParams.class);
		process(httpBean, requestParams);
	}

	private void process(HttpBean httpBean, RequestParams requestParams)
	{
		if (requestParams != null)
		{
			try
			{
				RequestParam[] params = requestParams.value();
				StringBuffer paramsLine = new StringBuffer();
				for (RequestParam param : params)
				{
					String name = param.name();
					String key = param.value();
					String paramValue = StringUtils.defaultString(PropertyUtil.getProperty(key), key);
					paramsLine.append(name).append("=").append(paramValue).append("&");
				}

				httpBean.setParams(paramsLine.toString());
			}
			catch (Exception e)
			{
				LOGGER.debug(e.getMessage());
				throw new RuntimeException(e.getMessage(), e.getCause());
			}
		}
	}
}
