package com.telstra.ncs.test.automation;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.telstra.ncs.test.automation.aspect.TestCaseAspect;
import com.telstra.ncs.test.automation.util.PropertyUtil;

/**
 * Created by David on 10/09/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:automation_test.xml" })
public abstract class AbstractRestJUnit
{
	public static final String PROFILE = System.getProperty("profile");

	public static final String STATUS_CODE_OK = "200";
	
	@Rule
	@Autowired
	public TestCaseAspect testCaseAspect;
	
	private String response;
	
	private String responseCode;
	
	@SuppressWarnings("unused")
	protected String getResponse()
	{
		return response;
	}
	
	@SuppressWarnings("unused")
	public void setResponse(String response)
	{
		this.response = response;
	}
	
	@SuppressWarnings("unused")
	protected String getResponseCode()
	{
		return responseCode;
	}
	
	@SuppressWarnings("unused")
	public void setResponseCode(String responseCode)
	{
		this.responseCode = responseCode;
	}
	
	protected <T> T transformJsonToObject(String json, Class<T> responseClass) throws IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, responseClass);
	}
	
	protected String getValue(String key) throws IOException
	{
		return PropertyUtil.getProperty(key);
	}
}
