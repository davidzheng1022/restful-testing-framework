package com.telstra.ncs.test.automation.annotationImpl;

import com.telstra.ncs.test.automation.domain.HttpBean;
import org.junit.runners.model.FrameworkMethod;

/**
 * Created by David on 11/09/2015.
 */
public interface UnitTestAnnotation
{
	void processAnnotationByMethod(HttpBean httpBean, FrameworkMethod method);
}
