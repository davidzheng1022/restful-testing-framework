package com.telstra.ncs.test.automation.annotationImpl;

import com.telstra.ncs.test.automation.domain.HttpBean;
import com.telstra.ncs.test.automation.annotation.RequestParam;
import com.telstra.ncs.test.automation.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.runners.model.FrameworkMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by David on 11/09/2015.
 */
@Service("requestParam")
public class RequestParamImpl implements UnitTestAnnotation
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestMappingImpl.class);

	public void processAnnotationByMethod(HttpBean httpBean, FrameworkMethod method)
	{
		RequestParam requestParam = method.getAnnotation(RequestParam.class);
		process(httpBean, requestParam);
	}

	private void process(HttpBean httpBean, RequestParam requestParam)
	{
		if (requestParam != null)
		{
			try
			{
				String param = requestParam.name();
				String key = requestParam.value();
				String paramValue = StringUtils.defaultString(PropertyUtil.getProperty(key), key);
				String params = param + "=" + paramValue + "&";
				httpBean.setParams(params);
			}
			catch (Exception e)
			{
				LOGGER.debug(e.getMessage());
				throw new RuntimeException(e.getMessage(), e.getCause());
			}
		}
	}
}
