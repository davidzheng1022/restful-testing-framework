package com.telstra.ncs.test.automation.util;

import com.telstra.ncs.test.automation.AbstractRestJUnit;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by David on 10/09/2015.
 */
public final class PropertyUtil
{
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertyUtil.class);

	private static Map<String, Properties> propertyMap = new HashMap();

	private static String path;

	private PropertyUtil() {

	}

	public static String getProperty(String file, String key) throws IOException
	{
		if (!StringUtils.isBlank(AbstractRestJUnit.PROFILE))
		{
			file = AbstractRestJUnit.PROFILE + "/" + file;
		}
		Properties propertyConfigurer;

		if (propertyMap.get(file) != null)
		{
			LOGGER.debug("loading the [" + file + "] from Cache.");
			propertyConfigurer = propertyMap.get(file);
		}
		else
		{
			propertyConfigurer = PropertiesLoaderUtils.loadProperties(new ClassPathResource(file));
			propertyMap.put(file, propertyConfigurer);
			LOGGER.debug("putting the [" + file + "] into Cache.");
		}

		String value = propertyConfigurer.getProperty(key);
		LOGGER.debug("Property key:value [" + key + ":" + value + "]");

		return value;
	}

	public static String getProperty(String key) throws IOException
	{
		for (Properties property : propertyMap.values())
		{
			if (!StringUtils.isBlank(property.getProperty(key)))
			{
				return property.getProperty(key);
			}
		}

		return null;
	}

	static
	{
		try
		{
			path = PropertyUtil.class.getClassLoader().getResource("").getPath();

			findFiles(".properties", path + AbstractRestJUnit.PROFILE, propertyMap);
		}
		catch (Exception e)
		{
			LOGGER.debug("Initializing all the property files");
			e.printStackTrace();
		}

	}

	private static void findFiles(String filenameSuffix, String currentDirUsed, Map<String, Properties> propertyMap)
			throws IOException
	{
		File dir = new File(currentDirUsed);
		if (!dir.exists() || !dir.isDirectory())
		{
			return;
		}

		for (File file : dir.listFiles())
		{
			if (file.getAbsolutePath().endsWith(filenameSuffix))
			{
				String fileName = "";
				if (!StringUtils.isBlank(AbstractRestJUnit.PROFILE))
				{
					fileName = AbstractRestJUnit.PROFILE + "/" + file.getName();
				}
				Properties propertyConfigurer = PropertiesLoaderUtils.loadProperties(new ClassPathResource(fileName));
				propertyMap.put(fileName, propertyConfigurer);
			}
		}

	}

	public static String getPath()
	{
		return path;
	}

	public static String getCurrentFolder()
	{
		return path + AbstractRestJUnit.PROFILE + "/";
	}
}
