package com.telstra.ncs.test.automation.aspect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import com.telstra.ncs.test.automation.annotationImpl.UnitTestAnnotation;
import com.telstra.ncs.test.automation.domain.HttpBean;

/**
 * Created by David on 10/09/2015.
 */
@Component
public class TestCaseAspect implements MethodRule
{
	
	@Autowired
	@Qualifier("requestBody")
	private UnitTestAnnotation requestBody;
	
	@Autowired
	@Qualifier("requestParam")
	private UnitTestAnnotation requestParam;
	
	@Autowired
	@Qualifier("requestParams")
	private UnitTestAnnotation requestParams;
	
	@Autowired
	@Qualifier("pathVariable")
	private UnitTestAnnotation pathVariable;
	
	@Autowired
	@Qualifier("requestMapping")
	private UnitTestAnnotation requestMapping;
	
	@Autowired
	@Qualifier("responseSchema")
	private UnitTestAnnotation responseSchema;
	
	protected RestOperations restTemplate = new RestTemplate();
	
	@Override
	public final Statement apply(final Statement base, final FrameworkMethod method, final Object target)
	{
		
		return new Statement()
		{
			@Override
			public void evaluate() throws Throwable
			{
				HttpBean httpBean = new HttpBean();
				requestMapping.processAnnotationByMethod(httpBean, method);
				requestParam.processAnnotationByMethod(httpBean, method);
				requestParams.processAnnotationByMethod(httpBean, method);
				pathVariable.processAnnotationByMethod(httpBean, method);
				requestBody.processAnnotationByMethod(httpBean, method);
				
				if (!StringUtils.isEmpty(httpBean.getEndPoint()) && !StringUtils.isEmpty(httpBean.getMethod()))
				{
					String endPoint = httpBean.getEndPoint();
					if (!StringUtils.isEmpty(httpBean.getParams()))
					{
						endPoint = endPoint + "?" + httpBean.getParams();
					}
					ResponseEntity<String> responseEntity =
							sendRequest(endPoint, httpBean.getPayload(), HttpMethod.valueOf(httpBean.getMethod()));
					int responseCode = responseEntity.getStatusCode().value();
					String response = responseEntity.getBody();
					httpBean.setResponse(response);
					responseSchema.processAnnotationByMethod(httpBean, method);
					
					setTargetValue(target, "setResponse", response);
					setTargetValue(target, "setResponseCode", String.valueOf(responseCode));
				}
				
				base.evaluate();
			}
		};
	}
	
	/**
	 * Post the json object and return the response.
	 *
	 * @param uri
	 * @param objectRequest
	 * @return
	 */
	public ResponseEntity<String> sendRequest(String uri, Object objectRequest, HttpMethod httpMethod)
	{
		HttpEntity<Object> requestEntity = assembleTheHttpEntity(objectRequest);
		
		ResponseEntity<String> jsonResponse;
		jsonResponse = restTemplate.exchange(uri, httpMethod, requestEntity, String.class);
		return jsonResponse;
	}
	
	/**
	 * Create the Http Entity with content-type:application/json, json object without null value.
	 *
	 * @param objectRequest
	 * @return
	 */
	private HttpEntity<Object> assembleTheHttpEntity(Object objectRequest)
	{
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		return new HttpEntity<Object>(objectRequest, headers);
	}
	
	private void setTargetValue(final Object target, String methodName, String value) throws NoSuchMethodException,
			InvocationTargetException, IllegalAccessException
	{
		Class[] cArg = new Class[1];
		cArg[0] = String.class;
		Method method = target.getClass().getMethod(methodName, cArg);
		method.invoke(target, new String[] { value });
	}
	
}
