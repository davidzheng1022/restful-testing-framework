package com.telstra.ncs.test.automation.annotationImpl;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonNodeReader;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.telstra.ncs.test.automation.annotation.ResponseSchema;
import com.telstra.ncs.test.automation.domain.HttpBean;
import com.telstra.ncs.test.automation.util.PropertyUtil;
import org.junit.runners.model.FrameworkMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by David on 10/09/2015.
 */
@Service("responseSchema")
public class ResponseSchemaImpl implements UnitTestAnnotation
{

	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseSchemaImpl.class);

	public void processAnnotationByMethod(HttpBean httpBean, FrameworkMethod method)
	{
		ResponseSchema jsonRespSchema = method.getAnnotation(ResponseSchema.class);
		if (jsonRespSchema != null)
		{
			boolean validate = validate(jsonRespSchema, httpBean.getResponse());
			Assert.isTrue(validate, "The response Json format is invalid:" + httpBean.getResponse());
		}
	}

	private boolean validate(ResponseSchema jsonRespSchema, String jsonResponse)
	{
		String location = jsonRespSchema.relativePath();

		JsonNode schema = null;
		JsonNode data = null;
		FileReader fileReader = null;
		ByteArrayInputStream byteArrayInputStream = null;
		try
		{
			fileReader = new FileReader(PropertyUtil.getPath() + location);
			byteArrayInputStream = new ByteArrayInputStream(jsonResponse.getBytes());
			schema = new JsonNodeReader().fromReader(fileReader);
			data = new JsonNodeReader().fromInputStream(byteArrayInputStream);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e.getMessage(), e.getCause());
		}
		finally
		{
			try
			{
				if (fileReader != null)
				{
					fileReader.close();
				}
				if (byteArrayInputStream != null)
				{
					byteArrayInputStream.close();
				}
			}
			catch (IOException e)
			{
				throw new RuntimeException(e.getMessage(), e.getCause());
			}

		}
		ProcessingReport report =
				JsonSchemaFactory.byDefault().getValidator().validateUnchecked(schema, data);
		LOGGER.error(report.toString());

		return report.isSuccess();
	}

}
